﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Windows.Forms;

namespace sha1_tool
{
    internal class Program
    {
        private static void AppendHash(string outputFile, byte[] hash)
        {
            using (var fs = new FileStream(outputFile, FileMode.Append, FileAccess.Write))
            using (var bw = new BinaryWriter(fs))
                bw.Write(hash);
        }

        private static byte[] CalculateHash(string inputFile, bool isPS3PKG = false)
        {
            using (FileStream streamR = new FileStream(inputFile, FileMode.Open, FileAccess.Read))
            using (SHA1 hasher = SHA1.Create())
            {
                if (!isPS3PKG)
                    return hasher.ComputeHash(streamR);

                int readBytes = 0;
                byte[] buffer = new byte[4096];

                while ((readBytes = streamR.Read(buffer, 0, buffer.Length)) > 0)
                {
                    if (streamR.Position >= streamR.Length)
                        readBytes -= 0x20;

                    hasher.TransformBlock(buffer, 0, readBytes, buffer, 0);
                }

                hasher.TransformFinalBlock(buffer, 0, 0);

                return hasher.Hash;
            }
        }

        private static void Error()
        {
            Console.WriteLine("Usage: {0} source.bin [result.bin|append|ps3pkg]" + Environment.NewLine, Application.ProductName);

            Environment.Exit(0);
        }

        private static void Main(string[] args)
        {
            if ((args.Length < 1) || (args.Length > 3))
                Error();

            if (!File.Exists(args[0]))
                Error();

            if ((args.Length == 1) && (args[0].ToLower() == "-version"))
            {
                Console.WriteLine(string.Format("{0}, written by {1} for catalinnc", Application.ProductName, Application.CompanyName));

                Environment.Exit(0);
            }
            else if (args.Length == 1)
                Error();

            byte[] hash;
            if (args[1].ToLower() == "ps3pkg")
            {
                UpdateNPDHash(args[0]);
                hash = CalculateHash(args[0], true);
            }
            else
                hash = CalculateHash(args[0]);

            if (args.Length == 2 && args[1].ToLower() == "append")
                AppendHash(args[0], hash);
            else if (args.Length == 2 && args[1].ToLower() == "ps3pkg")
                UpdateHash(args[0], hash);
            else
                WriteHash(args[1], hash);
        }

        private static void UpdateHash(string outputFile, byte[] hash)
        {
            using (var fs = new FileStream(outputFile, FileMode.Open, FileAccess.ReadWrite))
            using (var bw = new BinaryWriter(fs))
            {
                fs.Position = fs.Length - 0x20;
                bw.Write(hash);
            }
        }

        private static void UpdateNPDHash(string outputFile)
        {
            using (var fs = new FileStream(outputFile, FileMode.Open, FileAccess.ReadWrite))
            using (var bw = new BinaryWriter(fs))
            using (SHA1 hasher = SHA1.Create())
            {
                fs.Position = 4;
                if (fs.ReadByte() != 0x80)
                {
                    fs.Position = 0;
                    byte[] buffer = new byte[0x80];

                    fs.Read(buffer, 0, buffer.Length);
                    byte[] headerHash = hasher.ComputeHash(buffer);

                    fs.Position = 0x80;
                    bw.Write(headerHash, 3, headerHash.Length - 4);
                }
            }
        }

        private static void WriteHash(string outputFile, byte[] hash)
        {
            using (FileStream streamW = new FileStream(outputFile, FileMode.Create, FileAccess.Write))
            using (BinaryWriter writer = new BinaryWriter(streamW))
                writer.Write(hash);
        }
    }
}
